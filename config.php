<?php

$GLOBAL_SETTINGS = [
    'dbtype' => 'mysqli',
    'dbfile' => 'localdb.db',
    'logger_slack' => '0',

    'working_directory' => $_SERVER['DOCUMENT_ROOT'],

    'sql_host' => 'localhost',
    'sql_port' => '3306',
    'sql_login' => 'root',
    'sql_password' => '',
    'sql_database' => 'myfr'
];


