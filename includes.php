<?php
include('config.php');
include('vendor/autoload.php');
spl_autoload_register(function($className) {
    $className = str_replace("\\", DIRECTORY_SEPARATOR, $className);
    /** @mute  */
    include_once $_SERVER['DOCUMENT_ROOT'] . '/' . "$className.php";
});

include('Routes/routes.php');
