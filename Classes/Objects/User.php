<?php

namespace Classes\Objects;
use Core\DBObject;

class User extends DBObject {
    protected static $table = 'users';
    protected $fields = ['login', 'password', 'salt'];
    public function __construct(){
        $this->triggersBefore['insert'][] = 'Classes\Objects\Triggers\User\BeforeInsert';
        $this->triggersBefore['update'][] = 'Classes\Objects\Triggers\User\BeforeUpdate';
        parent::__construct(static::$table, $this->fields);
    }
}
