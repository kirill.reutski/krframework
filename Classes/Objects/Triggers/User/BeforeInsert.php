<?php


namespace Classes\Objects\Triggers\User;

use Core\DBObjectTriggerInterface;

class BeforeInsert implements DBObjectTriggerInterface{
    public function __construct()
    {
    }

    public function run(array &$data)
    {
        if (isset($data['password'])){
            $data['password'] = md5($data['password']);
        }
    }
}

