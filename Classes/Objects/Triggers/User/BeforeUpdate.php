<?php


namespace Classes\Objects\Triggers\User;


use Core\DBObjectTriggerInterface;

class BeforeUpdate implements DBObjectTriggerInterface{

    public function __construct()
    {
    }

    public function run(array &$data)
    {
        if (strlen($data['salt']) == 0) {
            $data['salt'] = md5($data['login'] . $data['password']);
        } else {
            $data['salt'] = md5($data['login'] . $data['password'] . $data['salt']);
        }
    }

}
