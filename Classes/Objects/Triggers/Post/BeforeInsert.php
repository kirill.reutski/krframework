<?php


namespace Classes\Objects\Triggers\Post;
use Core\DBObjectTriggerInterface;

class BeforeInsert implements DBObjectTriggerInterface{
    public function run(array &$data){
        $adding = 'trigger handled before insert';
        if (isset($data['content']) == false) {
            $data['content'] = $adding;
        } else {
            $data['content'] .= $adding;
        }
    }

    public function __construct(){}
}
