<?php


namespace Classes\Objects;
use Core\DBObject;


class Post extends DBObject
{
    protected static $table = 'posts';
    public function __construct()
    {
        $fields = ['subject', 'content'];
        $this->triggersBefore['insert'][] = 'Classes\Objects\Triggers\Post\BeforeInsert';
        parent::__construct(static::$table, $fields);
    }
}
