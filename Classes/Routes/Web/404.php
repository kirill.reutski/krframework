<?php
namespace Routes\Web;
use Core\InboundRequest;
use Core\TemplateProcessor;
class pageNotFound {
    public static function main(InboundRequest $request){
        $template = new TemplateProcessor('404');

        $template->render();
    }
}
