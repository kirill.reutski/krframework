<?php
/*
 * Вход
 *
 *
 */
namespace Classes\Routes\Web;
use Classes\Objects\User;
use Core\DBObject;
use Core\DBObjectCollection;
use Core\InboundRequest;
use Core\TemplateProcessor;
use Core\Auth;

class Login {
    public static function main(InboundRequest $request){
        $alreadyLoggedIn = Auth::isUserLoggedIn();
        if ($alreadyLoggedIn){
            $request->responseHTML = "You are logged in! <a href='/logout'>Log out</a>";
        } else {
            $template = new TemplateProcessor('login_page');
            $request->responseHTML = $template->getHTML();

        }

        return $request;
    }

    public static function logout(InboundRequest $request){
        Auth::logoutUser();
        $request->redirectUrl = '/login';
        return $request;
    }

    /**
     * @param Request $request
     * @return Request
     */
    public static function handler(InboundRequest $request){
        $out = '<pre>';
        $post_data = $request->body;
        //echo password_hash($post_data['password'], PASSWORD_DEFAULT);
        $user = new User();
        $user->findBy('login', $post_data['login']);
        //$out .= json_encode($user->dump());
        $password_valid = Auth::validateCredentials($post_data['login'], $post_data['password']);

        if ($password_valid) {
            Auth::loginUser($post_data['login'], ['someData' => 'datahere']);
            $request->redirectUrl = '/login';
        } else {
            $out .= "LOGIN FAILED!";
        }
        $request->responseHTML = $out;
        return $request;
    }
}
