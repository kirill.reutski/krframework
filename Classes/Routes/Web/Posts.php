<?php


namespace Classes\Routes\Web;
use Core\Auth;
use Core\InboundRequest;
use Classes\Objects\Post;
use Core\Router;
use Core\TemplateProcessor;
use mysql_xdevapi\Exception;
use Validators\StringValidator;
use Validators\ValidationException;

class Posts
{
    public static function list(InboundRequest $request){
        $user_info = Auth::isUserLoggedIn() ? Auth::getLoggedInUserInfo() : Router::redirectTo("/login");
        $amount = 10;
        if (isset($request->body['num'])) $amount = $request->body['num'];
        $posts = Post::take($amount);
        $body = '';
        $postData = [];
        foreach ($posts as $post){
            $postData[] = $post->dump();
        }
        $posts_json = json_encode($postData);
        $template = new TemplateProcessor('posts', [
            'title'=>'posts',
            'posts'=>$posts,
            'posts_json' => $posts_json,
            'loggedIn' => true
        ]);

        return $request->withHTML($template->getHTML());
    }

    public static function addView(InboundRequest $request){
        $user_info = Auth::isUserLoggedIn() ? Auth::getLoggedInUserInfo() : Router::redirectTo("/login");
        $out = new TemplateProcessor('post_add_view', ['title'=>'Add new post']);

        return $request->withHTML($out);
    }

    public static function editView(InboundRequest $request, $post_id){
        $user_info = Auth::isUserLoggedIn() ? Auth::getLoggedInUserInfo() : Router::redirectTo("/login");
        $out = new TemplateProcessor('post_add_view', ['title'=>'Edit post (id: ' . $post_id . ')']);

        return $request->withHTML($out);
    }

    public static function editHandler(InboundRequest $request, $post_id){
        $user_info = Auth::isUserLoggedIn() ? Auth::getLoggedInUserInfo() : Router::redirectTo("/login");
        $data = static::postCreateValidator($request->body);

        if ($data != null && $post_id > 0){
            var_dump('data is ok');
            try {
                $post = new Post();
                $post->findById($post_id);
                $post->cast($data);
                $result = $post->save();
                if ($result){
                    $request->redirectUrl = '/posts';
                    return $request;
                }
                else return $request->withHTML('ERROR saving post');
            } catch (\Core\DBException $e){
                return $request->withHTML('Post not found');
            }
            catch (Exception $e){
                var_dump($e);
            }

        } else {
            throw new ValidationException('Error with post data');
        }
    }

    public static function addHandler(InboundRequest $request){
        $user_info = Auth::isUserLoggedIn() ? Auth::getLoggedInUserInfo() : Router::redirectTo("/login");
        $data = static::postCreateValidator($request->body);
        if ($data != null){
            var_dump('data is ok');
            try {
                $post = new Post();
                $post->cast($data);
                $result = $post->save();
                if ($result){
                    $request->redirectUrl = '/posts';
                    return $request;
                }
                else return $request->withHTML('ERROR saving post');
            } catch (Exception $e){
                var_dump($e);
            }
        } else {
            throw new ValidationException('Error with post data');
        }
    }

    public static function postCreateValidator($params){
        $subject = '';
        $content = '';
        if (isset($params['subject'])) $subject = (string) $params['subject'];
        if (isset($params['content'])) $content = (string) $params['content'];

        if (StringValidator::simple($subject, 5, 100) && StringValidator::simple($content, 50, 10*1000)){
            return [
                'subject' => $subject,
                'content' => $content
            ];
        } else {
            return null;
        }
    }
}
