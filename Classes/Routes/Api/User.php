<?php
namespace Classes\Routes\Api;
use Core\InboundRequest;

class User extends \Classes\Objects\User {

  public static function get(InboundRequest $request, $slug){
    // $request->debug[] = 'UserRoutes::get worked';
    // $request->debug[] = $request->args;
    return $request->withJson([])->withCode(200);
  }

  public static function test(InboundRequest $inboundRequest){
      return $inboundRequest->withJson(['status'=>'OK', 'data'=>['type'=>'api']]);
  }

}
