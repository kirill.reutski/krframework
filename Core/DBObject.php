<?php

namespace Core;
use ArrayAccess;
use Exception;
use Validators\ValidationException;

class DBObject implements ArrayAccess
{
  protected static $table = '';
  protected $fields = [];
  protected $data = [];
  protected $id = 0;
  protected $triggersBefore = [
      'update'  => [],
      'delete'  => [],
      'insert'  => [],
  ];
  protected $triggersAfter  = [
      'update'  => [],
      'delete'  => [],
      'insert'  => [],
  ];
  protected $last_query = '';

    public function __construct(string $table, array $fields = []){
        self::$table = $table;
        $this->fields = $fields;
        if (count($fields) > 0) $this->fields[] = 'id';
    }

    public function getLastQuery(){
        return $this->last_query;
    }


    public function __get($field){
      if (isset($this->data[$field])) return $this->data[$field];
      return null;
    }

    public function __set($field, $value){
        $this->data[$field] = $value;
    }

    public function getFields(){
      return $this->fields;
    }

    /**
     * @param $value
     * @return bool
     * @throws DBException
     */
    public function findById($value){
      return $this->findBy('id', $value, 0, 1);
    }


    /**
     * @throws DBException
     * @throws DBObjectException
     */
    public function refresh(){
        if ($this->id > 0){
            $this->findById($this->id);
        } else {
            throw new DBObjectException('Tried to refresh DBObject without id!');
        }
    }

    /**
     * @param $column
     * @param $value
     * @param int $start
     * @param int $limit
     * @return bool
     * @throws DBException
     */
    public function findBy($column, $value, $start = 0, $limit = 1){
        //var_dump(['findBy'=>$this->fields]);
        if ($this->objectReady() == false) {
          return false;
        }
        $fields = ' * ';
        if (count($this->fields) > 0) $fields = implode(', ', $this->fields);
        $query = "SELECT $fields FROM " . static::$table . " WHERE $column = '$value' ";

        if ($limit > 0) {
          $query .= " LIMIT $start, $limit";
        }

        $this->last_query = $query;

        $result = DB::selectRAW($query);
        if (count($result) > 0) {
          $this->id = $result[0]['id'];
          foreach ($result[0] as $field => $value){
              if (count($this->fields) > 0){
                  if (in_array($field, $this->fields)){
                      $this->data[$field] = $value;
                  }
              } else {
                  $this->data[$field] = $value;
              }

          }
        }
        return false;
    }

    /**
     * @param $data
     * @throws DBObjectException
     */
    public function cast($data){
      if (count($this->fields) == 0) {
          $this->data = $data;
      } else {
          foreach ($data as $field => $value){
              if (in_array($field, $this->fields)){
                  $this->data[$field] = $value;
              }
          }
          if (isset($data['id'])){
              $this->id = (int) $data['id'];
          } else {
              throw new DBObjectException('failed to cast, id is not provided!');
          }
      }
    }

    /**
     * @param $field
     * @param $param1
     * @param null $param2
     * @return DBObjectCollection
     */
    public static function where($field, $param1, $param2 = null){

      $operator = '=';
      $value = $param1;
      if ($param2 !== null){
            $operator = $param1;
            $value = $param2;
      }
      $table = static::$table;
      $ret = new DBObjectCollection(get_called_class(),$table, $field, $operator, $value);
      return $ret;
    }

    /**
     * @param int $num
     * @param int $offset
     * @return DBObjectCollection
     * @throws DBObjectException
     * @throws DBException
     */
    public static function take($num = 1, $offset = 0){
        $table = static::$table;
        $ret = new DBObjectCollection(get_called_class(),$table, 'id', '>', '0');
        $ret->take($num, $offset);

        return $ret;
    }

    public function runTrigger($when, $action){
        $triggers_list = [];
        if ($when == 'before') $triggers_list = $this->triggersBefore;
        if ($when == 'after') $triggers_list = $this->triggersAfter;
        if (isset($triggers_list[$action])){
            foreach ($triggers_list[$action] as $triggerName){
                /** @var DBObjectTriggerInterface $trigger*/
                $trigger = new $triggerName;
                $trigger->run($this->data);
            }
        }
    }

    /**
     * @param bool $dry_run
     * @return bool|string
     * @throws DBException
     */
    public function save($dry_run = false){

        if ($this->objectReady() == false) {
            return false;
        }
        if ($this->id > 0){
            // handle triggers
            $this->runTrigger('before', 'update');

            $query = "UPDATE " . static::$table . " SET ";
            $pairs = [];
            foreach ($this->data as $field => $value) {
                $pairs[] = "$field = '$value'";
            }

            $query .= implode(', ', $pairs);
            $query .= " WHERE id = " . $this->id;
            $this->last_query = $query;
            if ($dry_run === false){
                $result = DB::updateRAW($query);
                if ($result >0) {
                    return true;
                }
                return false;
            } else {
                return $query;
            }

        } else {
            // handle triggers
            $this->runTrigger('before', 'insert');

            $fields = array_keys($this->data);
            $values = array_values($this->data);
            foreach ($values as &$value) $value = "'" . str_replace("'", "\'", $value) . "'";
            $query = "INSERT into " . static::$table . " (" . implode(', ', $fields ) . ") VALUES (" . implode(', ', $values) . ")";
            $this->last_query = $query;

            if ($dry_run === false){
                $result = DB::insertRAW($query);
                if ($result > 0) {
                    $this->id = $result;
                    return true;
                }
                //throw new DBException()
                return false;
            } else {
                return $query;
            }

        }
    }

    /**
     * @throws DBObjectException
     */
    public function delete(){
        if ($this->objectReady() == false) {
            throw new DBObjectException('Object is not ready!');
        }
        if ($this->id > 0){
            // handle triggers
            $this->runTrigger('before', 'delete');
            $table =  static::$table;
            $id = 'id';
            /** @mute  */
            $query = "DELETE from $table WHERE $id = " . $this->id;

            $this->last_query = $query;
            DB::deleteRAW($query);
            $this->runTrigger('after', 'update');

        } else {
            throw new DBObjectException('Tried to delete object without id!');
        }
    }



  private function objectReady(){
    if (strlen(static::$table) == 0) return false;
    return true;
  }

  public function dump(){
    return $this->data;
  }

    /**
     * Whether a offset exists
     * @link https://php.net/manual/en/arrayaccess.offsetexists.php
     * @param mixed $offset <p>
     * An offset to check for.
     * </p>
     * @return bool true on success or false on failure.
     * </p>
     * <p>
     * The return value will be casted to boolean if non-boolean was returned.
     * @since 5.0.0
     */
    public function offsetExists($offset)
    {
        return isset($this->data[$offset]);
    }

    /**
     * Offset to retrieve
     * @link https://php.net/manual/en/arrayaccess.offsetget.php
     * @param mixed $offset <p>
     * The offset to retrieve.
     * </p>
     * @return mixed Can return all value types.
     * @since 5.0.0
     */
    public function offsetGet($offset)
    {
        return isset($this->data[$offset]) ? $this->data[$offset] : null;
    }

    /**
     * Offset to set
     * @link https://php.net/manual/en/arrayaccess.offsetset.php
     * @param mixed $offset <p>
     * The offset to assign the value to.
     * </p>
     * @param mixed $value <p>
     * The value to set.
     * </p>
     * @return void
     * @since 5.0.0
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->data[] = $value;
        } else {
            $this->data[$offset] = $value;
        }
    }

    /**
     * Offset to unset
     * @link https://php.net/manual/en/arrayaccess.offsetunset.php
     * @param mixed $offset <p>
     * The offset to unset.
     * </p>
     * @return void
     * @since 5.0.0
     */
    public function offsetUnset($offset)
    {
        unset($this->data[$offset]);
    }

    /*
     *
     * Methods for quick api building
     *
     */

    /**
     * @param InboundRequest $inboundRequest
     * @param $id
     * @return InboundRequest
     * @throws DBException
     */
    public static function _get(InboundRequest $inboundRequest, $id){

        $called_class = get_called_class();
        /** @var $obj DBObject */
        $obj = new $called_class;
        $obj->findById($id);
        $out = $obj->dump();
        $allowed_fields = $obj->getFields();
        if (count($allowed_fields) > 0){
            $filtered = [];
            foreach ($out as $field => $value){
                if (in_array($field, $allowed_fields)){
                    $filtered[$field] = $value;
                }
            }

            $out = $filtered;
        }

        return $inboundRequest->withJson($out);
    }

    /**
     * @param InboundRequest $inboundRequest
     * @return InboundRequest
     */
    public static function _list(InboundRequest $inboundRequest){
        $called_class = get_called_class();
        $limit = 100;
        $offset = 0;

        if (isset($inboundRequest->body['limit'])) $limit = (int) $inboundRequest->body['limit'];
        if (isset($inboundRequest->body['offset'])) $offset = (int) $inboundRequest->body['offset'];
        /** @var $objects DBObjectCollection */
        /**@mute  */
        $objects = $called_class::take($limit, $offset);
        $out = $objects->dump();
        return $inboundRequest->withJson($out);
    }

    public static function _update(InboundRequest $inboundRequest, $id){

    }

    /**
     * @param InboundRequest $inboundRequest
     * @return InboundRequest
     * @throws DBException
     * @throws DBObjectException
     * @throws ValidationException
     */
    public static function _create(InboundRequest $inboundRequest){
        $called_class = get_called_class();
        /** @var $obj DBObject */
        $obj = new $called_class;
        $allowed_fields = $obj->getFields();
        $filtered_values = [];
        foreach ($allowed_fields as $field){
            if (isset($inboundRequest->body[$field])){
                $filtered_values[$field] = trim($inboundRequest->body[$field]);
            } else if ($field != 'id') {
                throw new ValidationException('Field ' . $field . ' is not passed!');
            }
        }

        if (count($filtered_values) == 0) throw new ValidationException('No fields passed for insertion!');

        foreach ($filtered_values as $field => $value){
            $obj[$field] = $value;
        }

        $result = $obj->save();
        if ($result > 0){
            $obj->refresh();
            $out = [
                'inserted' => true,
                'data' => $obj->dump()
            ];

            return $inboundRequest->withJson($out);
        } else {
            throw new DBObjectException('Failed to insert object! ' . $called_class . ', ' . json_encode($obj->dump()));
        }
    }

    public static function _delete(InboundRequest $inboundRequest, $id){

    }


}

class DBObjectException extends Exception {}
