<?php
namespace Core;
class Router {
  private $debug = false;
  private $types = [
    'get' => '__GET',
    'post' => '__POST'
  ];
  private $fallback = 'Core\DefaultFallback::call';
  private $beforeMiddlewares = [];
  private $afterMiddlewares = [];

  private $restricted = ['__GET', '__POST'];
  private $routes = [];

  private $callOptions = [];

  public function __construct() {}
  public function setRequestDebug(bool $a){$this->debug = $a;}
  public function setFallback(string $fallback){$this->fallback = $fallback; }
  public function getRoutes(){return $this->routes;}

    /**
     * @param string $url
     * @param string $method
     * @throws RouteException
     */
    public function get(string $url, string $method){
      $this->buildRoute('get', $url, $method);
  }

    /**
     * @param string $url
     * @param string $method
     * @throws RouteException
     */
    public function post(string $url, string $method){
      $this->buildRoute('post', $url, $method);
  }

  public function addBeforeMiddleware(string $middlewareHander){
    $this->beforeMiddlewares[] = $middlewareHander;
  }

  public function addAfterMiddleware(string $middlewareHander){
    $this->afterMiddlewares[] = $middlewareHander;
  }

  public function run(){
    if (isset($_SERVER['REQUEST_URI']) && isset($_SERVER['REQUEST_METHOD'])){
      $request = $this->buildRequest();

      $parts = explode('/', $request->path);
      $handler = $this->getHandler($parts, $request);
      $parameters = [&$request];
      try {

          foreach ($this->beforeMiddlewares as $middleware){
            call_user_func_array($middleware, $parameters);
          }
          foreach ($this->callOptions as $name => $option) {
              $parameters[$name] = $option;
          }
          if (is_callable($handler)){
              //var_dump(['calling'=>$handler, 'options'=>$parameters]);
              $request_final = call_user_func_array($handler, $parameters);
          } else {
              throw new RouteException('Handler ' . $handler . ' not found!');
          }


          $parameters = [&$request_final];

          foreach ($this->afterMiddlewares as $middleware){
              call_user_func_array($middleware, $parameters);
          }


          if (gettype($request_final) != 'object' ){
              throw new RouteException('Handler returned null response: ' . gettype($request_final));
          }
          $this->handleResponse($request_final);
      } catch (\Exception $e){
        $this->handleExceptionResponse($e);
      }
    }
  }

  private function handleExceptionResponse(\Exception $e){
    http_response_code(500);
    header('Content-Type: application/json');
    echo json_encode(['status'=>'error', 'exception'=>$e->getMessage()]);
  }

    /**
     * @param Request $request
     * @throws RouteException
     */
    private function handleResponse(InboundRequest $request){

        if (strlen($request->redirectUrl) >0) {
            header("Location: " . $request->redirectUrl);
        } else {
            if ($request->isJSON){
                http_response_code($request->responseCode);
                header('Content-Type: application/json');
                if ($this->debug){
                    $response = array_merge($request->responseData, ['debug'=>$request->debug]);
                } else {
                    $response = $request->responseData;
                }
                $response = ['status'=>'OK', 'data'=>$response];
                echo json_encode($response);
            } else {
                http_response_code($request->responseCode);
                echo $request->responseHTML;
            }
        }
  }

  private function buildRequest(){
    $request = new InboundRequest();
    return $request;
  }

  private function getHandler(array $parts, InboundRequest $request) : string {
    $method = $request->method;
    $current_array = &$this->routes;
    $fallback = false;
    foreach ($parts as $key => $part){
      if ($part === '') continue;

      if (isset($current_array[$part])){
        $current_array = &$current_array[$part];
        continue;
      } else {

        // case when we have smth like {slug} or {id}
        $hasBrackets = false;
        $bracketOption = '';
        foreach ($current_array as $option => $list){
          if (strpos($option, '{') > -1 && strpos($option, '}') > -1){
            $this->storeOption($option, $part);
            $hasBrackets = true;
            $bracketOption = $option;
            break;
          }
        }

        if ($hasBrackets){
          $current_array = &$current_array[$bracketOption];

          continue;
        } else {
          $fallback = true;
          break;
        }
      }
    }
    if ($fallback){
        return $this->fallback;
    }

    if ($method == 'POST' && isset($current_array['__POST'])){
      return $current_array['__POST'];
    } else if ($method == 'GET' && isset($current_array['__GET'])){
      return $current_array['__GET'];
    } else {
      return $this->fallback;
    }
  }

  private function storeOption($name, $value){
    $finalName = str_replace('{', '', $name);
    $finalName = str_replace('}', '', $finalName);
    $this->callOptions[$finalName] = $value;
  }

    /**
     * @param string $type
     * @param string $url
     * @param string $method
     * @throws RouteException
     */
    private function buildRoute(string $type, string $url, string $method){
    $parts = explode('/', $url);
    $current_array = &$this->routes;
    $count = count($parts);
    foreach ($parts as $key => $part){
      if ($part === "") continue;

      if (in_array($part, $this->restricted)) {
        throw new RouteException('Restricted path!');
      }

      if (isset($current_array[$part])){
        $current_array = &$current_array[$part];
        if ($key !== $count -1)
          continue;
      } else {
        $current_array[$part] = [];
        $current_array = &$current_array[$part];
      }

      if ($key == $count -1) {
        $type = $this->types[$type];
        $current_array[$type] = $method;
      }
    }
  }

    public static function redirectTo($string){
        header("Location: " . $string);
        die();
    }
}


class DefaultFallback {
    public static function call($parameters){
        $request = new InboundRequest();
        if (isset($parameters->path) && strpos($parameters->path, '/api/')){
            return $request->withJson(['Error'=>'Page not found'])->withCode(404);
        } else {
            $template = new TemplateProcessor('404');
            $template->apply_template();
            $out = $template->getHTML();
            return $request->withHTML($out);
        }
    }
}

class RouteException extends \Exception {}
