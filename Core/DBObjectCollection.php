<?php

namespace Core;

use ArrayAccess;
use Countable;
use Iterator;

    class DBObjectCollection implements Iterator, ArrayAccess, Countable {
        private $position;
        private $array = [];
        private $type = '';
        private $field = 'id';
        private $operator = '>';
        private $table = '';
        private $value = null;
        private $last_query = '';
        public function __construct($type, $table, $field, $operator, $value){
            $this->type = $type;
            $this->position = 0;
            $this->operator = $operator;
            $this->value = $value;
            $this->field = $field;
            $this->table = $table;
        }

        public function getLastQuery(){
            return $this->last_query;
        }

        /**
         * @throws DBException
         * @throws DBObjectException
         */
        public function save(){
            if (count($this->array) == 0) throw new DBObjectException('Tried to save empty list!');
            $queries = [];
            foreach ($this->array as $object){
                $queries[] = $object->save(DRY_RUN);
            }

            DB::commitOrDie($queries);
        }

        public function dump(){
            $out = [];
            foreach ($this->array as $obj){
                $out[] = $obj->dump();
            }

            return $out;
        }

        /**
         * @param int $num
         * @param int $offset
         * @return $this
         * @throws DBException
         * @throws DBObjectException
         */
        public function take($num = 1, $offset = 0){
            if ($this->value == null) $this->value = 0;
            $query = "SELECT * FROM " . $this->table . " WHERE " . $this->field . " " . $this->operator . " '" . $this->value . "' ";
            $query .= " LIMIT $offset, $num";
            //var_dump($query);
            $this->last_query = $query;

            try {
                $result = DB::selectRAW($query);
            } catch (DBException $e) {
                throw $e;
            }
            if ($result)
            foreach ($result as $item){
                /* @var DBObject $castedItem */
                $castedItem = new $this->type;
                $castedItem->cast($item);
                $this->array[] = $castedItem;
            }

            return $this;
        }
    /**
     * Return the current element
     * @link https://php.net/manual/en/iterator.current.php
     * @return mixed Can return any type.
     * @since 5.0.0
     */
    public function current()
    {
        return $this->array[$this->position];
    }

    /**
     * Move forward to next element
     * @link https://php.net/manual/en/iterator.next.php
     * @return void Any returned value is ignored.
     * @since 5.0.0
     */
    public function next()
    {
        ++$this->position;
    }

    /**
     * Return the key of the current element
     * @link https://php.net/manual/en/iterator.key.php
     * @return mixed scalar on success, or null on failure.
     * @since 5.0.0
     */
    public function key()
    {
        return $this->position;
    }

    /**
     * Checks if current position is valid
     * @link https://php.net/manual/en/iterator.valid.php
     * @return bool The return value will be casted to boolean and then evaluated.
     * Returns true on success or false on failure.
     * @since 5.0.0
     */
    public function valid()
    {
        return isset($this->array[$this->position]);
    }

    /**
     * Rewind the Iterator to the first element
     * @link https://php.net/manual/en/iterator.rewind.php
     * @return void Any returned value is ignored.
     * @since 5.0.0
     */
    public function rewind()
    {
        $this->position = 0;
    }

    /**
     * Whether a offset exists
     * @link https://php.net/manual/en/arrayaccess.offsetexists.php
     * @param mixed $offset <p>
     * An offset to check for.
     * </p>
     * @return bool true on success or false on failure.
     * </p>
     * <p>
     * The return value will be casted to boolean if non-boolean was returned.
     * @since 5.0.0
     */
    public function offsetExists($offset)
    {
        return isset($this->array[$offset]);
    }

    /**
     * Offset to retrieve
     * @link https://php.net/manual/en/arrayaccess.offsetget.php
     * @param mixed $offset <p>
     * The offset to retrieve.
     * </p>
     * @return mixed Can return all value types.
     * @since 5.0.0
     */
    public function offsetGet($offset)
    {
        return isset($this->array[$offset]) ? $this->array[$offset] : null;
    }

    /**
     * Offset to set
     * @link https://php.net/manual/en/arrayaccess.offsetset.php
     * @param mixed $offset <p>
     * The offset to assign the value to.
     * </p>
     * @param mixed $value <p>
     * The value to set.
     * </p>
     * @return void
     * @since 5.0.0
     */
    public function offsetSet($offset, $value)
    {
        //var_dump(get_class($value));
        if (is_null($offset)) {
            $this->array[] = $value;
        } else {
            $this->array[$offset] = $value;
        }
    }

    /**
     * Offset to unset
     * @link https://php.net/manual/en/arrayaccess.offsetunset.php
     * @param mixed $offset <p>
     * The offset to unset.
     * </p>
     * @return void
     * @since 5.0.0
     */
    public function offsetUnset($offset)
    {
        unset($this->array[$offset]);
    }

        /**
         * Count elements of an object
         * @link https://php.net/manual/en/countable.count.php
         * @return int The custom count as an integer.
         * </p>
         * <p>
         * The return value is cast to an integer.
         * @since 5.1.0
         */
        public function count()
        {
            return count($this->array);
        }
    }
