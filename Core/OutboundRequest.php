<?php


namespace Core;

use Exception;

class OutboundRequest{
    private $url = null;
    private $data = [];
    private $method = 'GET';
    private $headers = [];
    private $data_type = 'json';
    private $result = '';
    private $result_code = 0;
    private $response_headers = [];
    
    public function __construct(){
        return $this;
    }

    /**
     * @return $this
     * @throws OutboundRequestException
     */
    public function send(){
        $data_string = json_encode($this->data);

        $ch = curl_init($this->url);
        if ($this->method == 'POST'){
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
            $this->headers['Content-Length'] = "" . strlen($data_string);
        }

        if ($this->data_type == 'json'){
            $this->headers['Content-Type'] = 'application/json';
        }

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $this->headers);
        curl_setopt($ch, CURLOPT_HEADER, true);
        $response = curl_exec($ch);
        $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
        $this->parseResponseHeaders(substr($response, 0, $header_size));
        $this->result = substr($response, $header_size);

        if (curl_errno($ch)) {
            $error_msg = curl_error($ch);
        }

        $this->result_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        if (isset($error_msg)) {
            throw new OutboundRequestException('Request error: ' . $error_msg);
        }
        curl_close($ch);
        return $this;
    }

    public function toJson(){
        return json_decode($this->result, true);
    }

    public function getResponseHeaders(){
        return $this->response_headers;
    }

    /**
     * @param $type
     * @return $this
     * @throws OutboundRequestException
     */
    public function setDataType($type){
        $allowed_types = ['json', 'form'];
        if (in_array($type, $allowed_types)){
            $this->data_type = $type;
        } else {
            throw new OutboundRequestException('Incorrect type! Allowed: ' . json_encode($allowed_types));
        }
        return $this;
    }

    public function setUrl($string){
        $this->url = $string;
        return $this;
    }

    public function setHeaders(array $headers){
        $this->headers = $headers;
        return $this;
    }

    public function setHeader($key, $value){
        $this->headers[$key] = $value;
        return $this;
    }

    public function setData(array $data){
        $this->data = $data;
        return $this;
    }

    /**
     * @param $method
     * @return $this
     * @throws OutboundRequestException
     */
    public function setMethod($method){
        $allowed_methods = ['GET', 'POST'];
        if (in_array($method, $allowed_methods)){
            $this->method = $method;
        } else {
            throw new OutboundRequestException('Incorrect method! Allowed: ' . json_encode($allowed_methods));
        }
        return $this;
    }

    private function parseResponseHeaders(string $string){
        $lines = explode("\n", $string);

        foreach ($lines as $line){
            if (strpos($line, ':')){
                $line_parts = explode(':', $line);
                $key = trim($line_parts[0]);
                $value = trim($line_parts[1]);
                if (strlen($key) > 0){
                    $this->response_headers[$key] = $value;
                }
            }
        }
    }
}

class OutboundRequestException extends Exception{}
