<?php


namespace Core;


interface DBObjectTriggerInterface{
    public function run(array &$data);
    public function __construct();
}
