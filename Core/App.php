<?php


namespace Core;


class App {
    private static $instance = null;
    private $con = null;
    private $errors = [];

    private function __construct() {
        // The expensive process (e.g.,db connection) goes here.
        global $GLOBAL_SETTINGS;
        $this->con = mysqli_connect(
            $GLOBAL_SETTINGS['sql_host'] . ":" . $GLOBAL_SETTINGS['sql_port'],
            $GLOBAL_SETTINGS['sql_login'],
            $GLOBAL_SETTINGS['sql_password']
        );

        mysqli_select_db($this->con, $GLOBAL_SETTINGS['sql_database']);
    }

    public function addError($module, $code, $text, $severity = 0){
        $this->errors[] = [
            'time' => microtime(true),
            'module' => $module,
            'code' => $code,
            'text' => $text,
            'severity' => $severity
        ];
    }

    public function getErrors(){
        return $this->errors;
    }

    public function getDB(){
        return $this->con;
    }
    public static function getInstance()
    {
        if (self::$instance == null)
        {
            self::$instance = new App();
        }
        return self::$instance;
    }
}
