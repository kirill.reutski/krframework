<?php
namespace Core;
class Auth {
    public static function loginUser($login, $session_params){
        session_start();
        foreach ($session_params as $field => $value){
            if ($field != 'login')
                $_SESSION[$field] = $value;
        }

        $_SESSION['login'] = $login;
    }

    public static function logoutUser(){
        session_start();
        session_unset();
        session_destroy();
    }

    public static function isUserLoggedIn(){
        session_start();
        if (isset($_SESSION['login']) && strlen($_SESSION['login']) > 0){
            return true;
        }

        return false;
    }

    public static function getLoggedInUserInfo(){
        return $_SESSION;
    }

    public static function validateCredentials($login, $password, $table = 'users'){
        try {
            /** @noinspection SqlResolve */
            $password_db = DB::selectSingleValueRAW("SELECT password FROM $table WHERE login = '$login' LIMIT 1");
        }
        catch (DBException $e){
            return false;
        }

        $valid = password_verify($password, $password_db);
        return $valid;
    }
}
