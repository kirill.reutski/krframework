<?php
namespace Core;
set_exception_handler("Core\Logger::exception");
set_error_handler("Core\Logger::error");

class Logger {
    public static function log(string $message, string $level = 'DEBUG'){
        $route = "Logs/";
        $filename = date("Y_m_d") . "_logs.txt";
        $messageParts = explode("\r\n", $message);
        $time = date("h:i:s");
        foreach ($messageParts as &$messagePart){
            $messagePart = "$level|$time|" . $messagePart;
        }

        $message = implode("\r\n", $messageParts);
        file_put_contents($route.$filename, $message."\r\n", FILE_APPEND);
    }

    public static function exception($e){
        $message = static::jTraceEx($e);
        $message = str_replace('/var/www/html/', '', $message);
        static::log($message, 'EXCEPTION');
    }

    public static function error($errno, $errstr, $errfile, $errline){
        global $db;
        $message = $errfile . ', Line: ' . $errline . '. Error: ' . $errstr . "\r\n" . static::getCodePart($errfile, $errline) . "\r\n\r\n";

        $e = new \Exception();
        $message .= static::jTraceEx($e);
        $message = str_replace('/var/www/html/', '', $message);
        static::log($message, 'ERROR');
    }

    public static function getCodePart($file, $line, $lines = 5){
        $file = file($file);
        $outLines = [];
        $outLines[] = "... \r\n";
        $startLine = $line-$lines;
        $startLine = $startLine < 0 ? 0 : $startLine;
        for ($i = $startLine; $i < $startLine + $lines * 2; $i ++){
            if (isset($file[$i])) {
                $outLines[] = $file[$i];
            }
        }
        $outLines[] = "... \r\n";

        return implode("", $outLines);
    }

    public static function jTraceEx($e, $seen=null) {
        $starter = $seen ? 'Caused by: ' : '';
        $result = array();
        if (!$seen) $seen = array();
        $trace  = $e->getTrace();
        $prev   = $e->getPrevious();
        $result[] = sprintf('%s%s: %s', $starter, get_class($e), $e->getMessage());
        $file = $e->getFile();
        $line = $e->getLine();

        while (true) {
            $current = "$file:$line";

            $args_add = isset($trace[0]) && isset($trace[0]['args']) ? json_encode($trace[0]['args']) : json_encode($trace);
            $result[] = sprintf(' at %s%s%s (%s%s%s)',
                    count($trace) && array_key_exists('class', $trace[0]) ? str_replace('\\', '.', $trace[0]['class']) : '',
                    count($trace) && array_key_exists('class', $trace[0]) && array_key_exists('function', $trace[0]) ? '.' : '',
                    count($trace) && array_key_exists('function', $trace[0]) ? str_replace('\\', '.', $trace[0]['function']) : '(main)',
                    $line === null ? $file : basename($file),
                    $line === null ? '' : ':',
                    $line === null ? '' : $line) . "| Args: " . $args_add;
            if (is_array($seen))
                $seen[] = "$file:$line";
            if (!count($trace))
                break;
            $file = array_key_exists('file', $trace[0]) ? $trace[0]['file'] : 'Unknown Source';
            $line = array_key_exists('file', $trace[0]) && array_key_exists('line', $trace[0]) && $trace[0]['line'] ? $trace[0]['line'] : null;
            array_shift($trace);
        }
        $result = join("\n", $result);
        if ($prev)
            $result  .= "\n" . static::jTraceEx($prev, $seen);

        return $result;
    }
}
