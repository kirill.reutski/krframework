<?php


namespace Core;

use Exception;

class DBException extends Exception{}
class DB {
    /**
     * @param $query
     * @return mixed
     * @throws DBException
     */
    public static function selectSingleValueRAW($query){
        $out = null;
        $result = mysqli_query(App::getInstance()->getDB(), $query);
        if ($result === false ) {
            throw new DBException(mysqli_error(App::getInstance()->getDB()));
        }
        $res = mysqli_fetch_row($result);
        var_dump($res);
        if ($res !== null)
        if (count($res) > 0){
            $out = $res[0];
        }
        return $out;
    }

    /**
     * @param $query
     * @return array|string
     * @throws DBException
     */
    public static function selectRAW($query){

        $result = mysqli_query(App::getInstance()->getDB(), $query);
        if ($result === false ) {
            throw new DBException(mysqli_error(App::getInstance()->getDB()));
        }
        return mysqli_fetch_all($result, MYSQLI_ASSOC);
    }

    /**
     * @param $query
     * @return int|string
     */
    public static function updateRAW($query){
      $result = mysqli_query(App::getInstance()->getDB(), $query);
      if ($result === false) return mysqli_error(App::getInstance()->getDB());
      return mysqli_affected_rows(App::getInstance()->getDB());
    }

    public static function deleteRAW($query){
        $result = mysqli_query(App::getInstance()->getDB(), $query);
        if ($result === false) return mysqli_error(App::getInstance()->getDB());
        return mysqli_affected_rows(App::getInstance()->getDB());
    }

    /**
     * @param $query
     * @return int|string
     * @throws DBException
     */
    public static function insertRAW($query){
      $result = mysqli_query(App::getInstance()->getDB(), $query);
      if ($result === false) throw new DBException(mysqli_error(App::getInstance()->getDB()));//return mysqli_error(App::getInstance()->getDB());
      return mysqli_insert_id(App::getInstance()->getDB());
    }

    /**
     * @param array $queries
     * @return bool
     * @throws DBException
     */
    public static function commitOrDie(array $queries){
        $con = App::getInstance()->getDB();
        mysqli_autocommit($con, FALSE);
        $results = [];

        foreach ($queries as $num=>$query){
            $results[$num] = mysqli_query($con, $query);
        }
        //mysqli_commit($con);

        foreach ($results as $num=>$result){
            if ($result === false){
                mysqli_rollback($con);
                Logger::log('Failed to commit: ' . $queries[$num]);
                throw new DBException('Failed to commit!');
            }
        }
        mysqli_commit($con);
        return true;
    }
}
