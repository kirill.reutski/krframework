<?php
namespace Core;
class JWT {
  public static $JWT_SETTINGS = [
    'route' => ['/public'],
    'secret' => 'YourSuperSecretCode',
    'header' => 'Authorization',
    'prefix' => 'Bearer'
  ];
  public static $header = ['typ' => 'JWT', 'alg' => 'HS256'];

  public static function middlewareBefore(InboundRequest &$request){
    // define if route is covered
    $covered = false;
    foreach (static::$JWT_SETTINGS['route'] as $route){
      if (strpos($request->path, $route) === 0) {
        $covered = true;
        break;
      }
    }

    if ($covered){
      // define if header exists
      $exists = false;
      $token = '';
      foreach ($request->headers as $header => $value){
        if ($header === static::$JWT_SETTINGS['header']){
          $exists = true;
          $token = trim(str_replace(static::$JWT_SETTINGS['prefix'], '', $value));
          break;
        }
      }



      if ($exists && strlen($token) > 0){
        $result = static::decode($token);
        if ($result !== false){
          $request->args['JWT'] = $result;
        } else {
          throw new JWTException('Token is invalid!');
        }
      } else {
        throw new JWTException('Token is invalid!');
      }
    }
    // define if header correct

  }

  public static function encode(array $params){
    $secret = static::$JWT_SETTINGS['secret'];

    $segments = [];
    $segments[] = static::urlsafeB64Encode(json_encode(static::$header));
    $segments[] = static::urlsafeB64Encode(json_encode($params));

    $string = implode('.', $segments);
    $success = hash_hmac('sha256', $string, $secret);
    $segments[] = $success;
    return implode('.',$segments);
  }

  public static function decode($token){
    $parts = explode('.', $token);

    // check header
    if ($parts[0] !== static::urlsafeB64Encode(json_encode(static::$header))) return false;
    $string = implode('.', [$parts[0], $parts[1]]);
    $secret = static::$JWT_SETTINGS['secret'];

    $success = hash_hmac('sha256', $string, $secret);
    if ($success !== $parts[2]) return false;

    $decodedData = json_decode(static::urlsafeB64Decode($parts[1]), true);
    return $decodedData;
  }

  public static function urlsafeB64Encode($input){
      return str_replace('=', '', strtr(base64_encode($input), '+/', '-_'));
  }
  public static function urlsafeB64Decode($input)
    {
        $remainder = strlen($input) % 4;
        if ($remainder) {
            $padlen = 4 - $remainder;
            $input .= str_repeat('=', $padlen);
        }
        return base64_decode(strtr($input, '-_', '+/'));
    }


}

class JWTException extends \Exception{}
