<?php
namespace Core;
class TemplateProcessor {
    private $template = '';
    private $content = '';
    private $vars = [];
    public function __construct($templateName, $vars = []){
        $this->template = $templateName . '.tpl.html';
        $this->vars = $vars;
        if (strlen($this->template) > 0) {
            $this->apply_template();
        }
    }
    function __toString(){
        return $this->content;
    }

    function apply_template(){
        // {{title}} — searches for $this->vars['titile']
        // {{posts with [PostPreview]}} —  — searches for $this->vars['pots'], iterates it passing to Snippet/PostPreview via TemplateProcessor
        // {{[Header]}} — adds Snippet/Header template via TemplateProcessor (all vars are passed to)
        // {{if (loggedIn) then <a href="/logout">Log out</a>}} — conditions true/false
        // TODO:
        //
        global $GLOBAL_SETTINGS;
        $working_directory = $GLOBAL_SETTINGS['working_directory'] . '/Templates/';
        $data = file_get_contents($working_directory . $this->template);

        $matches = [];
        preg_match_all("/{{[^'}]+}}/", $data, $matches, PREG_SET_ORDER);

        foreach ($matches as $match){
            $snippetIteratorNeedle = ' with [';
            $isSnippetIteratorPos = strpos($match[0], $snippetIteratorNeedle);

            $snippetIncludeNeedle = 'include [';
            $isSnippetIncludePos = strpos($match[0], $snippetIncludeNeedle);

            $snippetIfNeedle = 'if (';
            $isSnippetIfPos = strpos($match[0], $snippetIfNeedle);

            if ($isSnippetIteratorPos > 0){// we should cycle the var through the snippet

                $snippetName = substr($match[0], $isSnippetIteratorPos + strlen($snippetIteratorNeedle), strpos($match[0], ']') - $isSnippetIteratorPos - strlen($snippetIteratorNeedle));
                $variableName = substr($match[0], 2, strpos($match[0], ' ')-2);
                $iterator = isset($this->vars[$variableName]) ? $this->vars[$variableName] : [];
                $snippetResultArray = [];
                foreach ($iterator as $item){

                    $itemPrepared = new TemplateProcessor("/Snippets/" . $snippetName, $item);
                    $snippetResultArray[] = $itemPrepared->getHTML();
                }
                $out = implode('<br/>', $snippetResultArray);
                $data = str_replace($match[0], $out, $data);

            } else if ($isSnippetIncludePos > 0){

                $snippetName = substr($match[0], $isSnippetIncludePos + strlen($snippetIncludeNeedle), strpos($match[0], ']') - $isSnippetIncludePos - strlen($snippetIncludeNeedle));
                $itemPrepared = new TemplateProcessor("/Snippets/" . $snippetName, $this->vars);
                $data = str_replace($match[0], $itemPrepared->getHTML(), $data);

            } else if ($isSnippetIfPos > 0){
                $reverse = false;
                $snippetName = substr($match[0], $isSnippetIfPos + strlen($snippetIfNeedle), strpos($match[0], ')') - $isSnippetIfPos - strlen($snippetIfNeedle));
                if ($snippetName[0] == '!'){
                    $snippetName = substr($snippetName, 1);
                    $reverse = true;
                }
                $startPos = strpos($match[0], ' then ') + 6;
                $valueIfTrue = substr($match[0], $startPos, strlen($match[0])-$startPos-2); // 2 for }}
                $valueIfFalse = '';
                if (isset($this->vars[$snippetName]) && $this->vars[$snippetName]){
                    $outValue = $reverse == false ? $valueIfTrue : $valueIfFalse;
                } else {
                    $outValue = $reverse == false ? $valueIfFalse : $valueIfTrue;
                }
                $data = str_replace($match[0], $outValue, $data);

            }  else {
                $value = $match[0];
                $value = str_replace("{{", "", $value);
                $value = str_replace("}}", "", $value);
                if (isset($this->vars[$value]) && in_array(gettype($this->vars[$value]), ['string', 'int', 'float', 'double'])){
                    $data = str_replace('{{' . $value . '}}', $this->vars[$value], $data);
                } else {
                    // nothing found at all, so we just remove the lexeme
                    $data = str_replace('{{' . $value . '}}', '', $data);
                }
            }
        }
        $this->content = $data;
    }

    function render(){
        echo $this->content;
    }

    function getHTML(){
        return $this->content;
    }


}
