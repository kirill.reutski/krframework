<?php
namespace Core;
class InboundRequest {
  public $method = '';
  public $path = '';
  public $args = [];
  public $body = [];
  public $debug = [];
  public $headers = [];
  public $redirectUrl = '';

  public $responseData = [];
  public $responseHTML = '';
  public $responseCode = 200;
  public $isJSON = false;
  public function __construct(){
      $method = $_SERVER['REQUEST_METHOD'];
      $this->method = $method;
      $parts = explode('?', $_SERVER['REQUEST_URI']);
      $path = $parts[0];//$_SERVER['REQUEST_URI'];
      $this->path = $path;
      $this->headers = getallheaders();
      if ($method == 'POST'){
          $bodyRAW = file_get_contents("php://input");
          $body = [];
          if (isset($this->headers['Content-Type'])){
              switch ($this->headers['Content-Type']){
                  case 'application/x-www-form-urlencoded':
                      parse_str($bodyRAW, $body);
                      break;
                  default:
                      $body = json_decode($bodyRAW, true);
              }
          }
          $this->body = $body;
      }

      foreach ($_GET as $field => $value){
          $this->body[$field] = $value;
      }
  }

    public function withJson(array $resp){
    $this->isJSON = true;
    $this->responseData = $resp;
    return $this;
  }

  public function withCode(int $code){
    $this->responseCode = $code;
    return $this;
  }

  public function withHTML(string $html){
    $this->responseHTML = $html;
    return $this;
  }
}
