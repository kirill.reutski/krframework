<?php


namespace Validators;


class StringValidator
{
    public static function simple($string, $min = 3, $max = 100){
        if ($min > 0 && $max > 0)
            return strlen($string) >=$min && strlen($string) <=$max;
        if ($min > 0)
            return strlen($string) >= $min;
        if ($max > 0)
            return strlen($string) <= $max;
    }

    public static function email($email){
        return filter_var($email, FILTER_VALIDATE_EMAIL);
    }
}
