<?php


namespace Tests;
use Core\App;

class TableGenerator
{
    public static function generateTable($table_name, array $fields){
        $con = App::getInstance()->getDB();
        $sql = "create table $table_name" . PHP_EOL .
            "(" . PHP_EOL .
            "    id int auto_increment," . PHP_EOL;
        foreach ($fields as $column => $type){
            $sql .= "    $column $type default null, ";
        }

        $sql .= "    created_at timestamp default current_timestamp null," . PHP_EOL .
            "    updated_at timestamp default current_timestamp on update current_timestamp, " . PHP_EOL .
            "    is_deleted bool default 0 null," . PHP_EOL .
            "    constraint " . $table_name . "_pk" . PHP_EOL .
            "        primary key (id)"  . PHP_EOL .
            ");";
        mysqli_query($con, $sql);

    }

    public static function deleteTable($table_name){
        $con = App::getInstance()->getDB();

        $sql = "DROP table $table_name; ";
        mysqli_query($con, $sql);
    }
}
