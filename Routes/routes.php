<?php
$router = new Core\Router();
// routes
include('api.php');
include('web.php');


// middlewares
//$router->addBeforeMiddleware('testMiddle::handle');
$router->addBeforeMiddleware('Core\JWT::middlewareBefore');
//$router->addAfterMiddleware('testMiddle::handleAfter');

//$router->setRequestDebug(true);

//running
$router->run();
