<?php
try {
    $router->get('/api/public/clients/get/{slug}', 'Classes\Routes\Api\User::get');
    $router->get('/api/public/clients/test', 'Classes\Routes\Api\User::test');
    $router->post('/api/public/clients/get/{slug}', 'Classes\Routes\Api\User::update');

    $router->get('/api/public/clients/{id}', 'Classes\Routes\Api\User::_get');
    $router->get('/api/public/clients', 'Classes\Routes\Api\User::_list');
    $router->post('/api/public/clients', 'Classes\Routes\Api\User::_create');
    //$router->get('/api/demo', 'Classes\Objects\Demo::init');

} catch (Core\RouteException $e){
    echo $e->getMessage();
}
