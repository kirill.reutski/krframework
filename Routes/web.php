<?php

try {
    $router->get('/login', 'Classes\Routes\Web\Login::main');
    $router->get('/logout', 'Classes\Routes\Web\Login::logout');
    $router->post('/login', 'Classes\Routes\Web\Login::handler');

    $router->get('/posts', 'Classes\Routes\Web\Posts::list');
    $router->get('/posts/add', 'Classes\Routes\Web\Posts::addView');
    $router->post('/posts/add', 'Classes\Routes\Web\Posts::addHandler');

    $router->get('/posts/{post_id}/edit', 'Classes\Routes\Web\Posts::editView');
    $router->post('/posts/{post_id}/edit', 'Classes\Routes\Web\Posts::editHandler');

    //$router->get('/demo', 'Classes\Objects\Demo::init');

} catch (Core\RouteException $e){

}
