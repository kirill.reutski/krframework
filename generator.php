<?php
include("config.php");
$action = '';
$param1 = '';
$param2 = '';
$param3 = '';
$actions_map = [
    'generate' => [
        'object' => 'generate_object'
    ]
];

if (isset($argc)) {
    for ($i = 0; $i < $argc; $i++) {
        if ($i == 1) $action = $argv[$i];
        else if ($i == 2) $param1 = $argv[$i];
        else if ($i == 3) $param2 = $argv[$i];
        else if ($i == 4) $param3 = $argv[$i];
    }
}

echo "Got: action — $action, param1 — $param1, param2 — $param2, param3 — $param3" . PHP_EOL;

if (in_array($action, array_keys($actions_map))){
    $available_p1 = $actions_map[$action];
    if (in_array($param1, array_keys($available_p1))){
        if (gettype($available_p1[$param1]) == 'string'){
            echo "Performing $action $param1 ...". PHP_EOL;
            if ($available_p1[$param1] == 'generate_object'){
                generate_object($param2, $param3);
            }
        } else {
            // may be third level..
        }
    } else {
        echo "Pass more parameters for action '$action'!" . PHP_EOL;
        echo "Available params: " . PHP_EOL;
        foreach (array_keys($available_p1) as $cmd) echo "$cmd" . PHP_EOL;
        die();
    }


} else {
    echo "Command '$action' not found!" . PHP_EOL;
    die();
}


function generate_object($object_name, $object_table_name){
    if (strlen($object_name) > 0){

        $myfile = fopen("Classes/Objects/$object_name.php", "w") or die("Unable to open file!");
        $txt =  "<?php" . PHP_EOL . PHP_EOL .
                "namespace Classes\Objects;" . PHP_EOL .
                "use Core\DBObject;" . PHP_EOL . PHP_EOL . PHP_EOL .
                "class $object_name extends DBObject {" . PHP_EOL .
                (strlen($object_table_name) ? 'protected static $table = "' . $object_table_name . '";' . PHP_EOL . PHP_EOL  : PHP_EOL).
                "}";
        fwrite($myfile, $txt);
        fclose($myfile);
        if (strlen($object_table_name) > 0){
            $sql = "create table $object_table_name" . PHP_EOL .
                "(" . PHP_EOL .
                "    id int auto_increment," . PHP_EOL .
                "    created_at timestamp default current_timestamp null," . PHP_EOL .
                "    updated_at timestamp default current_timestamp on update current_timestamp, " . PHP_EOL .
                "    is_deleted bool default 0 null," . PHP_EOL .
                "    constraint " . $object_table_name . "_pk" . PHP_EOL .
                "        primary key (id)"  . PHP_EOL .
                ");";
            global $GLOBAL_SETTINGS;
            mysqli_query($GLOBAL_SETTINGS['con'], $sql);
        }

    } else {
        echo "Please specify name as a third parameter. " . PHP_EOL;
        die();
    }
}
